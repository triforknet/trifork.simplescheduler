using System;

namespace Trifork.SimpleScheduler
{
    public class DailyJob : IJob
    {
        private readonly Action _action;

        public DailyJob(Action action)
        {
            _action = action;
        }

        public bool IsPending()
        {
            throw new System.NotImplementedException();
        }

        public void Run()
        {
            _action();
        }
    }
}