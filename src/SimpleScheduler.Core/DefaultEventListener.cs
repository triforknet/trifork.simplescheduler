using System;

namespace Trifork.SimpleScheduler
{
    public class DefaultEventListener : ISchedulerEventListener
    {
        public void OnBeforeRun(Type jobType)
        {
            
        }

        public void OnException(Type getType, Exception exception)
        {
            
        }

        public void OnSuccessfulRun(Type jobType)
        {
            
        }

        public void OnFinishedRun(Type jobType)
        {
            
        }
    }
}