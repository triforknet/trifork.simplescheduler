using System.Collections.Generic;
using System.Linq;

namespace Trifork.SimpleScheduler
{
    public class DefaultJobManager : IJobManager
    {
        private readonly List<IJob> _jobs = new List<IJob>();

        public IEnumerable<IJob> GetPendingJobs()
        {
            return _jobs.Where(x => x.IsPending());
        }

        public IEnumerable<IJob> Jobs
        {
            get { return _jobs; }
        }

        public void AddJob(IJob job)
        {
            _jobs.Add(job);
        }
    }
}