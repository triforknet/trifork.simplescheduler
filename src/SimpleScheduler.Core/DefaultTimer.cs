﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Trifork.SimpleScheduler
{
    public class DefaultTimer : ITimer
    {
        private readonly TimeSpan _interval;
        public event EventHandler Elapsed;

        private Task _worker;
        private CancellationTokenSource _cancellation;

        /// <summary>
        /// Initializes the timer with a default interval of 1 second.
        /// </summary>
        public DefaultTimer() : this(TimeSpan.FromSeconds(1))
        {
            
        }

        /// <summary>
        /// Initialzes the timer with the provided interval.
        /// </summary>
        /// <param name="interval">Time between each elapse.</param>
        public DefaultTimer(TimeSpan interval)
        {
            _interval = interval;
        }

        public void Start()
        {
            _cancellation = new CancellationTokenSource();
            var cancellationToken = _cancellation.Token;

            _worker = new Task(() =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    OnElapsed();
                    Thread.Sleep(_interval);
                }
            });
            
            _worker.Start();
        }

        private void OnElapsed()
        {
            var handler = Elapsed;

            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public void Stop()
        {
            if (_cancellation != null)
            {
                _cancellation.Cancel();
            }

            if (_worker != null)
            {
                _worker.Wait();

                _worker.Dispose();
                _worker = null;
            }

            if (_cancellation != null)
            {
                _cancellation.Dispose();
                _cancellation = null;
            }
        }
    }
}