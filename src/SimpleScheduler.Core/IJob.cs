namespace Trifork.SimpleScheduler
{
    public interface IJob
    {
        bool IsPending();
        void Run();
    }
}