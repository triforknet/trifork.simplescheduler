using System.Collections.Generic;

namespace Trifork.SimpleScheduler
{
    public interface IJobManager
    {
        IEnumerable<IJob> GetPendingJobs();
        void AddJob(IJob job);
    }
}