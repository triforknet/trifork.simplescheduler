using System;

namespace Trifork.SimpleScheduler
{
    public interface ISchedulerEventListener
    {
        void OnBeforeRun(Type jobType);
        void OnException(Type getType, Exception exception);
        void OnSuccessfulRun(Type jobType);
        void OnFinishedRun(Type jobType);
    }
}