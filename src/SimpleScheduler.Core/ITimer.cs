using System;

namespace Trifork.SimpleScheduler
{
    public interface ITimer
    {
        event EventHandler Elapsed;
        
        void Start();
        void Stop();
    }
}