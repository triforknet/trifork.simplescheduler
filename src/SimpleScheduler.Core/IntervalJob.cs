using System;

namespace Trifork.SimpleScheduler
{
    public class IntervalJob : IJob
    {
        private readonly TimeSpan _interval;
        private readonly Action _action;
        private DateTime? _lastRun;

        public IntervalJob(TimeSpan interval, Action action)
        {
            _interval = interval;
            _action = action;
        }

        public bool IsPending()
        {
            if (!_lastRun.HasValue)
            {
                return true;
            }

            var elapsedSinceLastRun = SystemTime.Now - _lastRun.Value;
            return elapsedSinceLastRun >= Interval;
        }

        public TimeSpan Interval
        {
            get { return _interval; }
        }

        public DateTime? LastRun
        {
            get { return _lastRun; }
        }

        public void Run()
        {
            _lastRun = SystemTime.Now;
            _action();
        }
    }
}