using System;

namespace Trifork.SimpleScheduler
{
    public class Scheduler
    {
        private readonly ITimer _timer;
        private readonly IJobManager _jobManager;
        private readonly ISchedulerEventListener _schedulerEventListener;

        public Scheduler() : this(new DefaultTimer(), new DefaultJobManager(), new DefaultEventListener())
        {
            
        }

        public Scheduler(ITimer timer, IJobManager jobManager, ISchedulerEventListener schedulerEventListener)
        {
            _timer = timer;
            _jobManager = jobManager;
            _schedulerEventListener = schedulerEventListener;

            CatchExceptions = true;
        }

        public ITimer Timer
        {
            get { return _timer; }
        }

        public IJobManager JobManager
        {
            get { return _jobManager; }
        }

        public bool CatchExceptions { get; set; }

        public void Start()
        {
            _timer.Elapsed += TimerOnElapsed;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
            _timer.Elapsed -= TimerOnElapsed;
        }

        private void TimerOnElapsed(object sender, EventArgs eventArgs)
        {
            var pendingJobs = _jobManager.GetPendingJobs();

            foreach (var job in pendingJobs)
            {
                try
                {
                    _schedulerEventListener.OnBeforeRun(job.GetType());

                    job.Run();

                    _schedulerEventListener.OnSuccessfulRun(job.GetType());
                }
                catch (Exception err)
                {
                    _schedulerEventListener.OnException(job.GetType(), err);

                    if (!CatchExceptions)
                    {
                        throw;
                    }
                }

                _schedulerEventListener.OnFinishedRun(job.GetType());
            }
        }
    }
}