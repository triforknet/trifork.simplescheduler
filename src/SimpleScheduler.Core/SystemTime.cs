using System;

namespace Trifork.SimpleScheduler
{
    public class SystemTime
    {
        private static Func<DateTime> _timeProvider = () => DateTime.Now;

        public static Func<DateTime> TimeProvider
        {
            set { _timeProvider = value; }
        }

        public static DateTime Now
        {
            get { return _timeProvider(); }
        }
    }
}