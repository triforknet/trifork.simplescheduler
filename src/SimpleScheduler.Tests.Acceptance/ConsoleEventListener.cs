﻿using System;

namespace Trifork.SimpleScheduler.Tests.Acceptance
{
    internal class ConsoleEventListener : ISchedulerEventListener
    {
        public void OnBeforeRun(Type jobType)
        {
            Console.WriteLine("{0}> executing...", jobType.Name);
        }

        public void OnException(Type getType, Exception exception)
        {
            Console.WriteLine("Error: {0}", exception);
        }

        public void OnSuccessfulRun(Type jobType)
        {
            
        }

        public void OnFinishedRun(Type jobType)
        {
            
        }
    }
}