﻿using System;

namespace Trifork.SimpleScheduler.Tests.Acceptance
{
    class Program
    {
        static void Main(string[] args)
        {
            int counter = 0;

            var timer = new DefaultTimer();
            timer.Elapsed += (sender, eventArgs) =>
            {
                counter++;

                Console.WriteLine("{0}> {1} begin", DateTime.Now.ToString("HH:mm:ss"), counter);
                Console.WriteLine("{0}> {1} end", DateTime.Now.ToString("HH:mm:ss"), counter);
            };

            timer.Start();

            Console.ReadLine();

            timer.Stop();



            Console.WriteLine("Press key to finish....");
            Console.ReadKey();
        }


//        static void Main(string[] args)
//        {
//            var scheduler = new Scheduler(new DefaultTimer(), new DefaultJobManager(), new ConsoleEventListener());
//
////            scheduler.JobManager.AddJob(new IntervalJob(TimeSpan.FromSeconds(1), () =>
////                {
////                    Console.WriteLine("{0}> every second", DateTime.Now.ToString("HH:mm:ss"));
////                }));
////
////            scheduler.JobManager.AddJob(new IntervalJob(TimeSpan.FromSeconds(2), () =>
////                {
////                    Console.WriteLine("{0}> every OTHER second", DateTime.Now.ToString("HH:mm:ss"));
////                }));
//
//            scheduler.JobManager.AddJob(new IntervalJob(TimeSpan.FromSeconds(1), () =>
//                {
//                    Console.WriteLine("throwing exception now...");
//                    throw new ApplicationException("ERROR!!!");
//                }));
//
//            scheduler.Start();
//
//            Console.ReadKey();
//
//            scheduler.Stop();
//        }
    }
}
