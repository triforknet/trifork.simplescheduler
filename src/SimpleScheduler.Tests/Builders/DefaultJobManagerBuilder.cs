namespace Trifork.SimpleScheduler.Tests.Builders
{
    internal class DefaultJobManagerBuilder
    {
        public DefaultJobManager Build()
        {
            return new DefaultJobManager();
        }
    }
}