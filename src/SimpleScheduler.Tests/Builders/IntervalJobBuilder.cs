using System;

namespace Trifork.SimpleScheduler.Tests.Builders
{
    internal class IntervalJobBuilder
    {
        private TimeSpan _interval;
        private Action _action;

        public IntervalJobBuilder()
        {
            _interval = new TimeSpan(ticks: 1);
            _action = () => { /* empty */ };
        }

        public IntervalJobBuilder WithInterval(TimeSpan interval)
        {
            _interval = interval;
            return this;
        }

        public IntervalJobBuilder WithAction(Action action)
        {
            _action = action;
            return this;
        }

        public IntervalJob Build()
        {
            return new IntervalJob(_interval, _action);
        }
    }
}