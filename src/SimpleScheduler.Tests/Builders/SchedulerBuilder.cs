using Moq;

namespace Trifork.SimpleScheduler.Tests.Builders
{
    internal class SchedulerBuilder
    {
        private ITimer _timer;
        private IJobManager _jobManager;
        private ISchedulerEventListener _schedulerEventListener;

        public SchedulerBuilder()
        {
            _timer = new Mock<ITimer>().Object;
            _jobManager = new Mock<IJobManager>().Object;
            _schedulerEventListener = new Mock<ISchedulerEventListener>().Object;
        }

        public SchedulerBuilder WithTimer(ITimer timer)
        {
            _timer = timer;
            return this;
        }

        public SchedulerBuilder WithJobManager(IJobManager jobManager)
        {
            _jobManager = jobManager;
            return this;
        }

        public SchedulerBuilder WithEventListener(ISchedulerEventListener schedulerEventListener)
        {
            _schedulerEventListener = schedulerEventListener;
            return this;
        }

        public Scheduler Build()
        {
            return new Scheduler(_timer, _jobManager, _schedulerEventListener);
        }
    }
}