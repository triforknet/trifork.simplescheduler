﻿using System;

namespace Trifork.SimpleScheduler.Tests
{
    internal class DailyJobBuilder
    {
        private Action _action;

        public DailyJobBuilder()
        {
            _action = () => { /* empty */ };
        }

        public DailyJobBuilder WithAction(Action action)
        {
            _action = action;
            return this;
        }

        public DailyJob Build()
        {
            return new DailyJob(_action);
        }
    }
}