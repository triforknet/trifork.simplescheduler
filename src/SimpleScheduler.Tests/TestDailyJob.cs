﻿using NUnit.Framework;

namespace Trifork.SimpleScheduler.Tests
{
    [TestFixture]
    public class TestDailyJob
    {
        [Test]
        public void is_instance_of_IJob()
        {
            var sut = new DailyJobBuilder().Build();
            Assert.IsInstanceOf<IJob>(sut);
        }

        [Test]
        public void run_executes_the_action()
        {
            var wasExecuted = false;

            var sut = new DailyJobBuilder()
                .WithAction(() => wasExecuted = true)
                .Build();

            sut.Run();

            Assert.IsTrue(wasExecuted);
        }
    }
}