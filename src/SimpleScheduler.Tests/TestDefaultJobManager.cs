﻿using Moq;
using NUnit.Framework;
using Trifork.SimpleScheduler.Tests.Builders;
using Trifork.SimpleScheduler.Tests.TestDoubles;

namespace Trifork.SimpleScheduler.Tests
{
    [TestFixture]
    public class TestDefaultJobManager
    {
        [Test]
        public void is_instance_of_IJobManager()
        {
            var sut = new DefaultJobManagerBuilder().Build();
            Assert.IsInstanceOf<IJobManager>(sut);
        }

        [Test]
        public void jobs_returns_expected_when_first_initialized()
        {
            var sut = new DefaultJobManagerBuilder().Build();
            CollectionAssert.IsEmpty(sut.Jobs);
        }

        [Test]
        public void jobs_returns_expected_when_a_single_job_is_added()
        {
            var sut = new DefaultJobManagerBuilder().Build();
            var dummyJob = new Mock<IJob>().Object;

            sut.AddJob(dummyJob);

            CollectionAssert.AreEquivalent(new[] {dummyJob}, sut.Jobs);
        }

        [Test]
        public void GetPendingJobs_returns_expected_when_no_jobs_are_added()
        {
            var sut = new DefaultJobManagerBuilder().Build();
            var result = sut.GetPendingJobs();

            CollectionAssert.IsEmpty(result);
        }

        [Test]
        public void GetPendingJobs_returns_expected_when_no_jobs_are_pending()
        {
            var sut = new DefaultJobManagerBuilder().Build();
            sut.AddJob(new FakeJob(isPending: false));

            var result = sut.GetPendingJobs();

            CollectionAssert.IsEmpty(result);
        }

        [Test]
        public void GetPendingJobs_returns_expected_when_single_job_is_pending()
        {
            var sut = new DefaultJobManagerBuilder().Build();
            var stubJob = new FakeJob(isPending: true);

            sut.AddJob(stubJob);

            var result = sut.GetPendingJobs();

            CollectionAssert.AreEquivalent(new[] {stubJob}, result);
        }

        [Test]
        public void GetPendingJobs_returns_expected_when_multiple_jobs_is_pending()
        {
            var sut = new DefaultJobManagerBuilder().Build();
            
            var stubJob1 = new FakeJob(isPending: true);
            var stubJob2 = new FakeJob(isPending: true);

            sut.AddJob(stubJob1);
            sut.AddJob(stubJob2);

            var result = sut.GetPendingJobs();

            CollectionAssert.AreEquivalent(new[] {stubJob1, stubJob2}, result);
        }
    }
}