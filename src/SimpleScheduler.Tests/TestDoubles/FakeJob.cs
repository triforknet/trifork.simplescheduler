using System;

namespace Trifork.SimpleScheduler.Tests.TestDoubles
{
    internal class FakeJob : IJob
    {
        private readonly bool _isPending;
        private readonly Action _action;

        public FakeJob(bool isPending = false, Action action = null)
        {
            _isPending = isPending;
            _action = action;
        }

        public bool IsPending()
        {
            return _isPending;
        }

        public void Run()
        {
            if (_action != null)
            {
                _action();
            }
        }
    }
}