using System;

namespace Trifork.SimpleScheduler.Tests.TestDoubles
{
    internal class FakeTimer : ITimer
    {
        public event EventHandler Elapsed;
        
        public void Start()
        {
            
        }

        public void Stop()
        {
            
        }

        public void OnElapsed()
        {
            var handler = Elapsed;
            
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}