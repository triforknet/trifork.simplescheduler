using System.Collections.Generic;

namespace Trifork.SimpleScheduler.Tests.TestDoubles
{
    internal class StubJobManager : IJobManager
    {
        private readonly IJob[] _pendingJobs;

        public StubJobManager(params IJob[] pendingJobs)
        {
            _pendingJobs = pendingJobs;
        }

        public IEnumerable<IJob> GetPendingJobs()
        {
            return _pendingJobs;
        }

        public void AddJob(IJob job)
        {
            
        }
    }
}