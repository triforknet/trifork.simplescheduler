﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Trifork.SimpleScheduler.Tests.Builders;

namespace Trifork.SimpleScheduler.Tests
{
    [TestFixture]
    public class TestIntervalJob
    {
        [SetUp]
        public void SetUp()
        {
            SystemTime.TimeProvider = () => DateTime.Now;
        }

        [TearDown]
        public void TearDown()
        {
            SystemTime.TimeProvider = () => DateTime.Now;
        }

        [Test]
        public void is_instance_of_IJob()
        {
            var sut = new IntervalJobBuilder().Build();
            Assert.IsInstanceOf<IJob>(sut);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(100)]
        [TestCase(9999)]
        public void interval_returns_expected(long ticks)
        {
            var expected = new TimeSpan(ticks);

            var sut = new IntervalJobBuilder()
                .WithInterval(expected)
                .Build();

            Assert.AreEqual(expected, sut.Interval);
        }

        [Test]
        public void when_initialized_IsPending_returns_expected()
        {
            var sut = new IntervalJobBuilder().Build();
            var result = sut.IsPending();
            
            Assert.IsTrue(result);
        }

        [Test]
        public void when_initialized_LastRun_returns_expected()
        {
            var sut = new IntervalJobBuilder().Build();
            Assert.IsNull(sut.LastRun);
        }

        [Test]
        public void run_sets_LastRun_as_expected()
        {
            var expectedDate = new DateTime(1);
            SystemTime.TimeProvider = () => expectedDate;

            var sut = new IntervalJobBuilder().Build();
            sut.Run();

            Assert.AreEqual(expectedDate, sut.LastRun);
        }

        [Test]
        public void IsPending_returns_expected_when_time_elapsed_since_last_run_is_less_than_interval()
        {
            var times = new Queue<DateTime>();
            times.Enqueue(DateTime.MinValue);
            times.Enqueue(DateTime.MinValue.AddHours(1));

            SystemTime.TimeProvider = () => times.Dequeue();

            var sut = new IntervalJobBuilder()
                .WithInterval(TimeSpan.FromHours(2))
                .Build();
            
            sut.Run();
            
            var result = sut.IsPending();

            Assert.IsFalse(result);
        }

        [Test]
        public void IsPending_returns_expected_when_time_elapsed_since_last_run_is_more_than_interval()
        {
            var times = new Queue<DateTime>();
            times.Enqueue(DateTime.MinValue);
            times.Enqueue(DateTime.MinValue.AddHours(2));

            SystemTime.TimeProvider = () => times.Dequeue();

            var sut = new IntervalJobBuilder()
                .WithInterval(TimeSpan.FromHours(1))
                .Build();
            
            sut.Run();
            
            var result = sut.IsPending();

            Assert.IsTrue(result);
        }

        [Test]
        public void IsPending_returns_expected_when_time_elapsed_since_last_run_equals_interval()
        {
            var times = new Queue<DateTime>();
            times.Enqueue(DateTime.MinValue);
            times.Enqueue(DateTime.MinValue.AddHours(1));

            SystemTime.TimeProvider = () => times.Dequeue();

            var sut = new IntervalJobBuilder()
                .WithInterval(TimeSpan.FromHours(1))
                .Build();
            
            sut.Run();
            
            var result = sut.IsPending();

            Assert.IsTrue(result);
        }

        [Test]
        public void run_invokes_expected_action()
        {
            var wasActionInvoked = false;

            var sut = new IntervalJobBuilder()
                .WithAction(() => wasActionInvoked = true)
                .Build();

            sut.Run();

            Assert.IsTrue(wasActionInvoked);
        }
    }
}