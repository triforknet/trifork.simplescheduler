﻿using System;
using Moq;
using NUnit.Framework;
using Trifork.SimpleScheduler.Tests.Builders;
using Trifork.SimpleScheduler.Tests.TestDoubles;

namespace Trifork.SimpleScheduler.Tests
{
    [TestFixture]
    public class TestScheduler
    {
        [Test]
        public void timer_returns_expected()
        {
            var dummyTimer = new Mock<ITimer>().Object;

            var sut = new SchedulerBuilder()
                .WithTimer(dummyTimer)
                .Build();

            Assert.AreSame(dummyTimer, sut.Timer);
        }

        [Test]
        public void jobsmanager_returns_expected()
        {
            var dummyJobManager = new Mock<IJobManager>().Object;

            var sut = new SchedulerBuilder()
                .WithJobManager(dummyJobManager)
                .Build();

            Assert.AreSame(dummyJobManager, sut.JobManager);
        }

        [Test]
        public void when_not_started_and_timer_elapsed_jobmanager_is_not_invoked()
        {
            var mockJobManager = new Mock<IJobManager>();
            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(mockJobManager.Object)
                .Build();

            fakeTimer.OnElapsed();

            mockJobManager.Verify(x => x.GetPendingJobs(), Times.Never);
        }

        [Test]
        public void when_started_and_timer_elapsed_jobmanager_is_invoked()
        {
            var mockJobManager = new Mock<IJobManager>();
            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(mockJobManager.Object)
                .Build();

            sut.Start();

            fakeTimer.OnElapsed();

            mockJobManager.Verify(x => x.GetPendingJobs());
        }

        [Test]
        public void when_stopped_and_timer_elapsed_jobmanager_is_not_invoked()
        {
            var mockJobManager = new Mock<IJobManager>();
            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(mockJobManager.Object)
                .Build();

            sut.Start();
            sut.Stop();

            fakeTimer.OnElapsed();

            mockJobManager.Verify(x => x.GetPendingJobs(), Times.Never);
        }

        [Test]
        public void when_started_timer_is_also_started()
        {
            var mockTimer = new Mock<ITimer>();

            var sut = new SchedulerBuilder()
                .WithTimer(mockTimer.Object)
                .Build();

            sut.Start();

            mockTimer.Verify(x => x.Start());
        }

        [Test]
        public void when_stopped_timer_is_also_stopped()
        {
            var mockTimer = new Mock<ITimer>();

            var sut = new SchedulerBuilder()
                .WithTimer(mockTimer.Object)
                .Build();

            sut.Stop();

            mockTimer.Verify(x => x.Stop());
        }

        [Test]
        public void runs_pending_job()
        {
            var wasJobRun = false;

            var fakeJob = new FakeJob(
                isPending: true,
                action: () => wasJobRun = true
                );

            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(new StubJobManager(fakeJob))
                .Build();

            sut.Start();
            fakeTimer.OnElapsed();

            Assert.IsTrue(wasJobRun);
        }

        [Test]
        public void runs_all_pending_jobs()
        {
            var runs = 0;
            Action spyAction = () => runs++;

            var fakeJobs = new[]
            {
                new FakeJob(isPending: true, action: spyAction),
                new FakeJob(isPending: true, action: spyAction),
                new FakeJob(isPending: true, action: spyAction),
            };

            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(new StubJobManager(fakeJobs))
                .Build();

            sut.Start();
            fakeTimer.OnElapsed();

            Assert.AreEqual(fakeJobs.Length, runs);
        }

        [Test]
        public void event_is_raised_when_job_throws_exception()
        {
            var mockSchedulerEventListener = new Mock<ISchedulerEventListener>();

            var errorProneJob = new FakeJob(
                isPending: true,
                action: () => { throw new Exception(); }
                );

            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(new StubJobManager(errorProneJob))
                .WithEventListener(mockSchedulerEventListener.Object)
                .Build();

            sut.Start();
            fakeTimer.OnElapsed();

            mockSchedulerEventListener.Verify(x => x.OnException(errorProneJob.GetType(), It.IsAny<Exception>()));
        }

        [Test]
        public void event_is_raised_before_job_is_executed()
        {
            var mockSchedulerEventListener = new Mock<ISchedulerEventListener>();

            var fakeJob = new FakeJob(
                isPending: true,
                action: () => { /* empty */ }
                );

            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(new StubJobManager(fakeJob))
                .WithEventListener(mockSchedulerEventListener.Object)
                .Build();

            sut.Start();
            fakeTimer.OnElapsed();

            mockSchedulerEventListener.Verify(x => x.OnBeforeRun(fakeJob.GetType()));
        }

        [Test]
        public void event_is_raised_on_successful_job_execution()
        {
            var mockSchedulerEventListener = new Mock<ISchedulerEventListener>();

            var fakeJob = new FakeJob(
                isPending: true,
                action: () => { /* empty */ }
                );

            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(new StubJobManager(fakeJob))
                .WithEventListener(mockSchedulerEventListener.Object)
                .Build();

            sut.Start();
            fakeTimer.OnElapsed();

            mockSchedulerEventListener.Verify(x => x.OnSuccessfulRun(fakeJob.GetType()));
        }

        [Test]
        public void event_is_raised_after_job_was_executed()
        {
            var mockSchedulerEventListener = new Mock<ISchedulerEventListener>();

            var fakeJob = new FakeJob(
                isPending: true,
                action: () => { /* empty */ }
                );

            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(new StubJobManager(fakeJob))
                .WithEventListener(mockSchedulerEventListener.Object)
                .Build();

            sut.Start();
            fakeTimer.OnElapsed();

            mockSchedulerEventListener.Verify(x => x.OnFinishedRun(fakeJob.GetType()));
        }

        [Test]
        public void catch_exceptions_returns_expected_when_initialized()
        {
            var sut = new SchedulerBuilder().Build();
            Assert.IsTrue(sut.CatchExceptions);
        }

        [Test]
        public void catches_exceptions()
        {
            var errorProneJob = new FakeJob(
                isPending: true,
                action: () => { throw new Exception(); }
                );

            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(new StubJobManager(errorProneJob))
                .Build();

            sut.CatchExceptions = true;

            sut.Start();

            Assert.DoesNotThrow(() => fakeTimer.OnElapsed());
        }

        [Test]
        public void throws_exception()
        {
            var errorProneJob = new FakeJob(
                isPending: true,
                action: () => { throw new Exception(); }
                );

            var fakeTimer = new FakeTimer();

            var sut = new SchedulerBuilder()
                .WithTimer(fakeTimer)
                .WithJobManager(new StubJobManager(errorProneJob))
                .Build();

            sut.CatchExceptions = false;

            sut.Start();

            Assert.Throws<Exception>(() => fakeTimer.OnElapsed());
        }
    }
}